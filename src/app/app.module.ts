import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import {AppMaterialModule} from './app-material/app-material.module';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../environments/environment';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import {GetDataService} from './get-data.service';
import {PostDataService} from './post-data.service';
import { AddRecipeDialogComponent } from './add-recipe-dialog/add-recipe-dialog.component';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    RecipeListComponent,
    AddRecipeDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    FormsModule
  ],
  entryComponents: [
    AddRecipeDialogComponent
  ],
  providers: [GetDataService, PostDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
