import {Component, OnInit} from '@angular/core';
import {GetDataService} from '../get-data.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {AddRecipeDialogComponent} from '../add-recipe-dialog/add-recipe-dialog.component';
import {Recipe} from '../recipe';
import {PostDataService} from '../post-data.service';
import {filter, tap} from 'rxjs/operators';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  public getData: any;

  constructor(private getDataService: GetDataService, public dialog: MatDialog, public postDataService: PostDataService) {
  }

  openAddDialog(): void {
    const dialogRef = this.dialog.open(AddRecipeDialogComponent, {
      width: '250px'
    });

    dialogRef.afterClosed()
      .pipe(filter(result => !!result))
      .subscribe((result: Recipe) => this.postDataService.addRecipe(result));

  }

  deleteElement(element) {
    this.postDataService.removeElement(element);
  }

  openEditDialog(recipe) {
    const dialogRef = this.dialog.open(AddRecipeDialogComponent, {
      width: '250px',
      data: {recipe}
    });

    dialogRef.afterClosed()
      .pipe(filter(result => !!result))
      .subscribe((result: Recipe) => this.postDataService.updateDialog(result));
  }

  ngOnInit() {
    this.getDataService.getDataFromFireBase().subscribe((item) => this.getData = item);
  }

}
