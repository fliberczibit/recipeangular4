import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs/Observable';
import {Recipe} from './recipe';
@Injectable()
export class PostDataService {
  public todoCollectionRef: AngularFirestoreCollection<any>;
  public todo$: Observable<any>;

  constructor(private afs: AngularFirestore) {
    this.todoCollectionRef = this.afs.collection('recipes');
    this.todo$ = this.todoCollectionRef.valueChanges();
  }


  addRecipe(recipe: Recipe) {
    this.todoCollectionRef.add(recipe);
  }

  removeElement(element) {
    this.todoCollectionRef.doc(element.id).delete();
  }

  updateDialog(elementToUpdate) {
    this.todoCollectionRef.doc(elementToUpdate.id).update({ name: elementToUpdate.name, ingredients: elementToUpdate.ingredients });
  }


}





