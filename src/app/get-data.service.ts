import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, DocumentChangeAction} from 'angularfire2/firestore';
import {Observable} from 'rxjs/Observable';
import {Recipe} from './recipe';
@Injectable()
export class GetDataService {
  public todoCollectionRef: AngularFirestoreCollection<any>;
  public recipeData$: Observable<Recipe[]>;
  public recipeDataWithID$: Observable<Recipe[]>;


  constructor(private afs: AngularFirestore) {
    this.todoCollectionRef = this.afs.collection<Recipe>('recipes');
    this.recipeData$ = this.todoCollectionRef.valueChanges();
    this.recipeDataWithID$ = this.todoCollectionRef.snapshotChanges()
      .map((actions) => {
        return actions.map( action => {
          const data = action.payload.doc.data() as Recipe;
          const id = action.payload.doc.id;
          return {id, ...data};
        });
      });
  }

  getDataFromFireBase() {
    return this.recipeDataWithID$;
    // return this.recipeData$;
  }
}
