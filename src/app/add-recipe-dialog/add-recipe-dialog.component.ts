import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Recipe} from '../recipe';
import {PostDataService} from '../post-data.service';


@Component({
  selector: 'app-add-recipe-dialog',
  templateUrl: './add-recipe-dialog.component.html',
  styleUrls: ['./add-recipe-dialog.component.css']
})
export class AddRecipeDialogComponent implements OnInit {
  public name: string;
  public ingredients: string;
  public showButtonToUpdateRecipe: boolean;
  private recipe: Recipe;

  constructor(private dialogRef: MatDialogRef<AddRecipeDialogComponent, Recipe>,
              @Inject(MAT_DIALOG_DATA) private data: { recipe: Recipe }) {
    this.recipe = this.data ? this.data.recipe : null;
  }

  ngOnInit() {
    if (this.recipe) {
      this.name = this.recipe.name;
      this.ingredients = this.recipe.ingredients.join(', ').replace(/\s/g, '');
      this.showButtonToUpdateRecipe = true;
    }
  }

  saveDialog() {
    if (this.name && this.ingredients) {
      this.dialogRef.close({
        ...this.recipe,
        name: this.name,
        ingredients: this.ingredients.replace(/\s/g, '').split(',')
      });
    } else {
      alert('Nie wypełniłeś pola');
    }
  }

  closeDialog() {
    this.dialogRef.close();
  }
}



